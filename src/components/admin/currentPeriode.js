import { gql } from '@apollo/client/core';
import client from 'siskom/apollo-client.js';

const query = gql`
  query CurrentPeriode {
    currentPeriode {
      akhir
      awal
      id
      semester
      tahun
    }
  }
`;
var currentPeriode = null;
export const key = {};
export async function getCurrentPeriode () {
  if (currentPeriode != null) {
    return currentPeriode;
  }
  const result = await client.query({ query });
  currentPeriode = result.data.currentPeriode;
  return currentPeriode;
}
