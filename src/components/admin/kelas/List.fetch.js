import ListGQL from 'siskom/graphql/list-kelas.gql'
import formatDay from 'siskom/commons/formatDay'
import { parse, format } from 'date-fns'
import addMinutes from 'date-fns/addMinutes'

function filterKelas (keyword) {
  return (kelas) => {
    const nama = kelas.mataKuliah.nama.toLowerCase();
    if (nama.includes(keyword)) return true;
    const kode = kelas.mataKuliah.kode.toLowerCase();
    if (kode.includes(keyword)) return true;
    return false;
  }
}

export default async function ({ client, idPeriode, keyword }) {
  const variables = { idPeriode };
  const query = ListGQL;
  const result = await client.query({ query, variables, fetchPolicy: 'network-only' })
  const periodes = result.data.allPeriodes.nodes.map(it => ({
    label: `${it.tahun}/${it.tahun + 1}, semester ${it.semester}`,
    value: it.id
  }));
  let classes = result.data.allKelas.nodes;
  if (keyword) {
    const lowerKeyword = keyword.toLowerCase();
    classes = classes
      .filter(filterKelas(lowerKeyword));
  }
  classes = classes.map(kelas => {
    if (kelas.schedule) {
      const sch = kelas.schedule;
      const t0 = parse(sch.waktuKul, 'HH:mm:ss', new Date());
      const t1 = addMinutes(t0, 120);
      const fday = formatDay(sch.hariKul);
      kelas.schedule.formatDayTime = `${fday}, ${format(t0, 'HH:mm')} - ${format(t1, 'HH:mm')}`;
    }
    return kelas;
  });
  return { classes, periodes };
}
