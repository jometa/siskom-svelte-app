export const CLASHES = 'siskom.admin.kelas.clashes'
export const FORM = 'siskom.admin.kelas.create.input'

export class ClashError extends Error {
  constructor (clashes) {
    super('terdapat tabrakan dosen atau ruangan')
    this.name = 'ClashError'
    this.clashes = clashes
  }
}
