import { writable } from 'svelte/store'

export const key = {}

const { subscribe, update } = writable({
  value: false,
  message: '',
  type: 'info'
})

export default {
  subscribe,
  show: ({ message, type }) => {
    update(obj => ({
      value: true,
      message,
      type
    }))
    setTimeout(() => {
      update(obj => ({
        ...obj,
        value: false
      }))
    }, 5000)
  }
}
