import { writable } from 'svelte/store';

export const periode = writable({
  tahun: 2007,
  semester: 2
});
export const phase = writable('REGISTRATION');
export const mahasiswa = writable(null);
export const dosen = writable(null);
export const user = writable(null);
