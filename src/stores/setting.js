import { writable } from 'svelte/store';
import { REGISTRATION, all as allPhase } from 'siskom/commons/phases'

const { subscribe, set, update } = writable({
  currentPeriode: {
    id: 0,
    awal: null,
    akhir: null,
    tahun: null,
    semester: null
  },
  phase: REGISTRATION
});

export default {
  subscribe,
  changePhase: (phase) => {
    return update(obj => ({
      ...obj,
      phase
    }))
  },
  changePeriode: (periode) => {
    return update(obj => ({
      ...obj,
      currentPeriode: periode
    }))
  }
};
