import 'regenerator-runtime/runtime'
import App from './components/App.svelte';
import './styles/index.css';

const app = new App({
  target: document.getElementById('root')
})

export default app;
