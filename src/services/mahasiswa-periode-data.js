import { gql } from '@apollo/client/core';
import apolloClient from 'siskom/apollo-client.js';

const RequiredMhsTranskripGQL = gql`
  query RequiredMhsTranskrip($idMahasiswa: Int!) {
    attendedPeriode (_idMahasiswa: $idMahasiswa) {
      nodes {
        id
        tahun
        semester
      }
    }

    currentPeriode {
      akhir
      awal
      id
      semester
      tahun
    }
  }
`;

export default async function mahasiswaPeriodeData (id) {
  const queryResult = await apolloClient.query({
    query: RequiredMhsTranskripGQL,
    variables: {
      idMahasiswa: id
    }
  });
  const { attendedPeriode, currentPeriode } = queryResult.data;
  const periodes = attendedPeriode.nodes.map((it, index) => {
    return {
      value: it.id,
      label: `semester ${index + 1}, ${it.tahun}/${it.tahun + 1}`
    }
  })
  return {
    periodes,
    currentPeriode
  };
}
