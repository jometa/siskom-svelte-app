import { gql } from 'apollo-boost';

const query = gql`
  query LoadAllPeriode {
    allPeriodes {
      nodes {
        id
        tahun
        semester
        awal
        akhir
      }
    }
  }
`;

export default async function ({ client, transform }) {
  const result = await client.query({ query });
  let items = result.data.allPeriodes.nodes;
  if (!items) {
    throw new Error('empty data');
  }
  if (transform) {
    return items.map(transform);
  }
  return items;
}