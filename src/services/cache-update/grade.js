import { gql } from '@apollo/client/core';

export default function ({ client, grade, idKelas, idMhs }) {
  client.writeFragment({
    id: `${idKelas}-${idMhs}`,
    fragment: gql`
      fragment mahasiswaKelas on MahasiswaKela {
        nilai
        __typename
      }
    `,
    data: {
      nilai: grade,
      __typename: 'MahasiswaKela'
    }
  })
}
