import apolloClient from 'siskom/apollo-client.js';
import CurrentPeriodeGQL from 'siskom/graphql/current-periode.gql';

export default function currentPeriode () {
  return apolloClient.query({
    query: CurrentPeriodeGQL
  }).then(result => result.data.currentPeriode)
}
