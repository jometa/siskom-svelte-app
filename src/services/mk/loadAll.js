import { gql } from 'apollo-boost';
import formatKodeMk from 'siskom/commons/formatKodeMk';

const SearchMkGQL = gql`
  query SearchMk {
    allMataKuliahs {
      nodes {
        id
        kode
        nama
        tipeMk
        scheduled
        openIn
      }
    }
  }
`;

export default async function loadAllMks ({ client }) {
  const options = { query: SearchMkGQL };
  const result = await client.query(options);

  if (!result.data || !result.data.allMataKuliahs) 
    throw new Error('empty data');

  const items = result.data.allMataKuliahs.nodes;
  const transformed = items.map(mk => ({
    ...mk,
    formattedKode: formatKodeMk(mk)
  }));

  return transformed;
}
