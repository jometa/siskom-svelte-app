import { gql } from '@apollo/client/core';
import apolloClient from 'siskom/apollo-client.js';
import * as stores from 'siskom/stores/index.js';

const GQL_CommonData = gql`
  query CommonData {
    setting: currentSetting {
      phase
      periode: currentPeriode {
        akhir
        awal
        id
        semester
        tahun
      }
    }
    dosen: currentDosen {
      id
      nama
      nip
      status
      sex
    }
    user: currentAppUser {
      id
      nodeId
      scopes
      targetId
      tipeUser
      username
    }
    mahasiswa: currentMahasiswa {
      id
      idPa
      nama
      nodeId
      nim
      sex
      status
      tahunMasuk
      pa: dosenByIdPa {
        id
        nama
        nip
        sex
        status
        nodeId
      }
    }
  }
`;

export default async function updateSessionData () {
  const result = await apolloClient.query({
    query: GQL_CommonData
  })
  const {
    dosen,
    user,
    mahasiswa,
    setting
  } = result.data;
  const {
    periode,
    phase
  } = setting;

  if (!phase) {
    throw new Error('empty phase');
  }
  if (!periode) {
    throw new Error('empty active periode'); 
  }

  stores.periode.set(periode);
  stores.phase.set(phase);
  stores.dosen.set(dosen);
  stores.mahasiswa.set(mahasiswa);
  stores.user.set(user);
}
