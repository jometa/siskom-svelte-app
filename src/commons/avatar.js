export default function (key, size) {
  return `https://api.adorable.io/avatars/${size}/${key}.png`
}
