import formatDay from 'siskom/commons/formatDay'

export default function (day, time) {
  const fday = formatDay(day);
  return `${fday}, ${time}`;
}