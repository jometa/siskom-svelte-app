export default function (periode) {
  return {
    label: `${periode.tahun}/${periode.tahun + 1}, semester ${periode.semester}`,
    value: periode.id
  };
}
