export default function (_keyword) {
  const keyword = _keyword.toUpperCase();
  return (mk) => {
    const nama = mk.nama.toUpperCase();
    if (nama.includes(keyword)) return true;
    const kode = mk.kode.toUpperCase();
    if (kode.includes(keyword)) return true;
    return false;
  }
}