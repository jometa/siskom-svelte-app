export default function (mk) {
  let kode;
  if (mk.tipeMk == 'UMUM') {
    kode = 'MPK';
  } else if (mk.tipeMk == 'WAJIB' || mk.tipeMk == 'PILIHAN') {
    kode = 'STKOM';
  } else {
    throw new Error(`unknown tipeMk = ${mk.tipeMk}`);
  }
  return kode;
}
