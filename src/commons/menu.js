export const mahasiswa = [
  { header: true, label: 'personal' },
  { label: 'Info', path: '/#/app/me_mhs/info' },
  { label: 'Jadwal', path: '/#/app/me_mhs/schedule' },
  { label: 'Transkrip Nilai', path: '/#/app/me_mhs/transkrip' },
  { label: 'KRS', path: '/#/app/me_mhs/krs' },
  { label: 'KHS', path: '/#/app/me_mhs/khs' },
  { header: true, label: 'ilkom' },
  { label: 'Dosen', path: '/#/app/ilkom/dosens' },
  { label: 'Mata Kuliah', path: '/#/app/ilkom/mks' },
  { label: 'Kelas', path: '/#/app/ilkom/kelas' },
  { header: true, label: 'lainnya' },
  { label: 'Bantuan', path: '/#/app/etc/help' },
  { label: 'Logout', path: '/#/app/auth/logout' }
];

export const dosen = [
  { header: true, label: 'personal' },
  { label: 'Info', path: '/#/app/me_dosen/info' },
  { label: 'Jadwal', path: '/#/app/me_dosen/schedules' },
  { label: 'Bimbingan Akademik', path: '/#/app/me_dosen/pas' },
  { header: true, label: 'ilkom' },
  { label: 'Kelas', path: '/#/app/ilkom/kelas' },
  { label: 'Dosen', path: '/#/app/ilkom/dosens' },
  { label: 'Mata Kuliah', path: '/#/app/ilkom/mks' },
  { label: 'Mahasiswa', path: '/#/app/ilkom/mahasiswas' },
  { header: true, label: 'lainnya' },
  { label: 'Bantuan', path: '/#/app/etc/help' },
  { label: 'Logout', path: '/#/app/auth/logout' }
];