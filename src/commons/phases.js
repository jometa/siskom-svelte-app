export const REGISTRATION = 'REGISTRATION'
export const SCHEDULING = 'SCHEDULING'
export const CLASS_PICK = 'CLASS_PICK'
export const KMB = 'KMB'
export const GRADING = 'GRADING'
export const REVISE = 'REVISE'

export const all = [
  REGISTRATION,
  SCHEDULING,
  CLASS_PICK,
  KMB,
  GRADING,
  REVISE
]
