export function load (key) {
  const raw = localStorage.getItem(key)
  if (raw) return JSON.parse(raw)
  return null
}

export function set (key, val, raw) {
  let item = raw ? val : JSON.stringify(val);
  localStorage.setItem(key, item)
}

export function remove (key) {
  localStorage.removeItem(key)
}